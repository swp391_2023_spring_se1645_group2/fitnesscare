/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.product;

import dao.impl.CategoryProductDAOImpl;
import dao.impl.ProductDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.CategoryProduct;
import model.Product;

/**
 *
 * @author ThinkPro
 */
public class ProductListController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private int record_per_page = 8;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            ProductDAOImpl pd = new ProductDAOImpl();
            // get all category
            ArrayList<CategoryProduct> listcp = new CategoryProductDAOImpl().getAllCategory();
            // get all product
            ArrayList<Product> list = pd.getAllProduct();
            // get index of page
            String indexPage = request.getParameter("index");
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);
            // Set category
            int cp_id = 0;
            String categoryId = "!= -1";
            String strCategoryId = request.getParameter("cp_id");
            if (strCategoryId != null) {
                cp_id = Integer.parseInt(strCategoryId);
                categoryId = "= " + strCategoryId;
            } 
            System.out.println(strCategoryId + " hihi");

            // Set key for search 
            String searchKey = "";
            String strSearchKey = request.getParameter("key");
            if (strSearchKey != null) {
                searchKey = strSearchKey;
            }
            // Set sort 
            String value = "update_date";
            String type = "";
            String strType = request.getParameter("type");
            String strValue = request.getParameter("value");
            if (strType != null) {
                type = strType;
            }
            if (strValue != null) {
                value = strValue;
            }

            // get total number of product
            int endPage = (pd.getTotalProduct() / record_per_page);
            if (pd.getTotalProduct() % record_per_page != 0) {
                endPage++;
            }

            // filter
            ArrayList<Product> listOfPage = pd.pagingProduct(index, record_per_page, searchKey, categoryId, type, value);

            // Set param request to jsp page    
            session.setAttribute("listProduct", list);
            session.setAttribute("listcp", listcp);
            session.setAttribute("historyUrl", "list-product");
            String history = "list-product?index=" + indexPage;
            if (strCategoryId != null) {
                history = history + "&cp_id=" + strCategoryId;
                request.setAttribute("historyCategoryId", "&cp_id=" + strCategoryId);
                request.setAttribute("cp_id", strCategoryId);
            }
            if (strSearchKey != null) {
                history = history + "&key=" + strSearchKey;
                request.setAttribute("historyKey", "&key=" + strSearchKey);
                request.setAttribute("key", strSearchKey);
            }
            if (strValue != null) {
                history = history + "&value=" + strValue;
                request.setAttribute("historyValue", "&value=" + strValue);
                request.setAttribute("value", strValue);
            }
            if (strType != null) {
                history = history + "&type=" + strType;
                request.setAttribute("historyType", "&type=" + strType);
                request.setAttribute("type", strType);
            }

            session.setAttribute("historyUrl", history);
            request.setAttribute("cp_id", cp_id);

            request.setAttribute("endPage", endPage);
            request.setAttribute("listOfPage", listOfPage);
            request.setAttribute("pageIndex", index);

            request.getRequestDispatcher("/view/productList.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
