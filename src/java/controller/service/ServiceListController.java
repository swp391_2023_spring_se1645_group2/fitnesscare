package controller.service;

import dao.impl.ServiceDAOImpl;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Service;
import dao.inter.IServiceDAO;


public class ServiceListController extends HttpServlet {

    
    private final int record_per_page = 4;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            IServiceDAO sd = new ServiceDAOImpl();
            // get all service
            ArrayList<Service> list = sd.getAllService();
            // get index of page
            String indexPage = request.getParameter("index");
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);

            // Set key for search 
            String searchKey = "";
            String strSearchKey = request.getParameter("key");
            if (strSearchKey != null) {
                searchKey = strSearchKey.trim();
            }
            // Set sort 
            String value = "service_name";
            String type = "";
            String strType = request.getParameter("type");
            String strValue = request.getParameter("value");
            if (strType != null) {
                type = strType;
            }
            if (strValue != null) {
                value = strValue;
            }

            // get total number of product
            int endPage = (sd.getTotalService()/ record_per_page);
            if (sd.getTotalService()% record_per_page != 0) {
                endPage++;
            }

            // filter
            ArrayList<Service> listOfPage = sd.pagingService(index, record_per_page, searchKey, type, value);

            // Set param request to jsp page    
            session.setAttribute("listService", list);
            session.setAttribute("historyUrl", "list-service");
            String history = "list-service?index=" + indexPage;
            
            if (strSearchKey != null) {
                history = history + "&key=" + strSearchKey;
                request.setAttribute("historyKey", "&key=" + strSearchKey);
                request.setAttribute("key", strSearchKey);
            }
            if (strValue != null) {
                history = history + "&value=" + strValue;
                request.setAttribute("historyValue", "&value=" + strValue);
                request.setAttribute("value", strValue);
            }
            if (strType != null) {
                history = history + "&type=" + strType;
                request.setAttribute("historyType", "&type=" + strType);
                request.setAttribute("type", strType);
            }

            session.setAttribute("historyUrl", history);

            request.setAttribute("endPage", endPage);
            request.setAttribute("listOfPage", listOfPage);
            request.setAttribute("pageIndex", index);

            request.getRequestDispatcher("/view/serviceList.jsp").forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
