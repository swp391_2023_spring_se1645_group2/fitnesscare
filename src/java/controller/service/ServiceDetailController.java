package controller.service;

import dao.impl.ServiceDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Service;
import dao.inter.IServiceDAO;

public class ServiceDetailController extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServiceDetailController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServiceDetailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        IServiceDAO sd = new ServiceDAOImpl();
        int service_id = Integer.parseInt(request.getParameter("service_id"));
        Service s = sd.getServiceByID(service_id);
        request.setAttribute("service", s);
        HttpSession session = request.getSession();
        String history = "service-detail?service_id="+service_id;
        session.setAttribute("historyUrl", history);
        request.getRequestDispatcher("/view/serviceDetail.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
