package dao.inter;

import java.util.ArrayList;
import model.CategoryProduct;

public interface ICategoryProductDAO {
    ArrayList<CategoryProduct> getProductCategories();
    CategoryProduct getCategoryByID(int cid);
    ArrayList<CategoryProduct> getAllCategory();
    
}
