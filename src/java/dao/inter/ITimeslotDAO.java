package dao.inter;

import java.util.ArrayList;
import model.Timeslot;

public interface ITimeslotDAO {
    ArrayList<Timeslot> getAllTimeslot();
}
