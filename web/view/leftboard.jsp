<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">          
            <div class="nav">
                <c:if test="${sessionScope.us.role_id == 1}">
                    <div class="sb-sidenav-menu-heading">Bảng điểu khiển</div>
                    <a class="nav-link" href="#">        
                        <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                        Thống kê
                    </a>
                </c:if>

                <div class="sb-sidenav-menu-heading">Quản lý</div>
                <c:if test="${sessionScope.us.role_id == 2 || sessionScope.us.role_id == 1}">
                    <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="false" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                        Quản lý người dùng
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapse1" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <c:if test="${sessionScope.us.role_id == 1}">
                                <a class="nav-link" href="managestaff">Quản lý nhân viên</a>
                            </c:if>
                            <a class="nav-link" href="managecustomer">Quản lý khách hàng</a>
                        </nav>
                    </div>     
                    <a class="nav-link" href="manageproductlist">        
                        <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                        Quản lý sản phẩm
                    </a>
                    <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                        Quản lý dịch vụ
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapse3" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="nav-link" href="manageservicelist">Danh sách dịch vụ</a>
                            <a class="nav-link" href="addService.jsp">Thêm mới dịch vụ</a>
                        </nav>
                    </div>
                    <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="false" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                        Quản lý blog
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapse4" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="nav-link" href="managebloglist">Danh sách blog</a>
                            <a class="nav-link" href="addBlog.jsp">Thêm mới blog</a>
                        </nav>
                    </div>
                    <c:if test="${sessionScope.us.role_id == 1}">
                    <a class="nav-link" href="managemateriallist">        
                        <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                        Quản lý vật tư
                    </a>
                    </c:if>

                    <a class="nav-link" href="#">        
                        <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                        Quản lý order
                    </a>
                    <div class="collapse" id="collapse5" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="nav-link" href="manageorder">Quản lý đơn hàng</a>
                        </nav>
                    </div>
                    
                    <a class="nav-link" href="registedService">        
                        <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                        Quản lý dịch vụ đã đăng ký
                    </a>
                </c:if>
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Đăng nhập bởi:</div>
            <b>${sessionScope.us.fullName}</b>
        </div>
    </nav>
</div>