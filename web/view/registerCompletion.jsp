<%-- 
    Document   : registerCompletion
    Created on : Feb 26, 2023, 4:00:41 PM
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="./assets/fonts/themify-icons/themify-icons.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <title>Register Completion</title>
    </head>
    <body>
        <div class="row" style="display: flex; margin-top: 50px">
            <div class="col-md-3">
            <img src="./images/cart-completion/checked.png" alt="" style="width: 100px; height: 100px; float: right"/>
        </div>
            <div class="col-md-9" style="margin-top: 25px">
            <h1>
                Hoàn tất đơn đăng ký dịch vụ.
            </h1>
            <div>
                Đơn đăng ký đang trong quá trình xét duyệt.<br>
                Quý khách sẽ nhận được thông báo khi đơn đăng ký xét duyệt thành công.<br>
                Quý khách vui lòng thanh toán và nhận thẻ dịch vụ tại trung tâm Fitnesscare.<br>
                Cảm ơn quý khách!
            </div><br>
            <a class="btn btn-primary" style="color: white" href="home" role="button">Quay lại Trang chủ</a>
        </div>
        </div>
        
    </body>
</html>
