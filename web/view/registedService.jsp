<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registed Service</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./assets/css/styles.css">
        <link rel="stylesheet" href="./assets/css/style.css">
        <link rel="stylesheet" href="./assets/fonts/themify-icons/themify-icons.css">
        <%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
        <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
        <%@include file="../component/javascript.jsp" %>
        <link rel="icon" type="image/x-icon" href="../images/logo.png">

        <style>
            li.nav-item a.active{
                color: blue;
                font-weight: bold;
            }
            li.nav-item a{
                color: black;

            }
            .form-select option{
                background-color: transparent;
            }

            .input-container {
                position: relative;
                display: inline-block;
            }

            .input-container input[type="text"] {
                padding-right: 30px;
            }

            .clear-icon {
                position: absolute;
                top: 50%;
                right: 10px;
                transform: translateY(-50%);
                cursor: pointer;
                display: none;
            }

            .input-container:hover .clear-icon {
                display: block;
            }
            .an {
                display: block;
                display: -webkit-box;
                height: 16px*1.3*3;
                font-size: 16px;
                line-height: 1.3;
                -webkit-line-clamp: 2;  /* số dòng hiển thị */
                -webkit-box-orient: vertical;
                overflow: hidden;
                text-overflow: ellipsis;
                margin-top:10px;
            }
        </style>
    </head>
    <body class="sb-nav-fixed">
        <div id="main">
        <%@include file="header-manage.jsp" %>
            <%@include file="../component/account.jsp" %>
            <div id="layoutSidenav">
                <%@include file="leftboard.jsp" %>
                <div id="layoutSidenav_content">

                    <div class="container-fluid" style="margin-top: 50px ">
                        <div class="row">
                            <h1 style="text-align: center;">Danh Sách Dịch Vụ Ðã Đăng Ký</h1>

                            <div class="row justify-content-between" style="margin-left: 50px; margin-right: 50px">

                                <div class="col-md-4">
                                    <form class="search-bar" action="registedService">
                                        <div class="input-container">
                                            <input id="myInput1" class="form-control"  type="text" name="key" value="${key}" placeholder="Tìm kiếm" >
                                            <span class="clear-icon" onclick="clearInput()">X</span>
                                        </div>
                                        <button type="submit" class="fa fa-search"></button>
                                    </form>
                                </div>
                                <div class="col-md-3">
                                    <select class="form-select" aria-label="Default select example" onchange="location = this.value;">                                                                     

                                        <option class=" text-center" value="registedService?${historyKey}&type=1" ${type eq "1" ? "Selected" : ""}>
                                            Tất cả
                                        </option>
                                        <option class=" text-center" value="registedService?${historyKey}&type=3" ${type eq "3" ? "Selected" : ""}>
                                            Chấp nhận
                                        </option>
                                        <option class="text-center " value="registedService?${historyKey}&type=4" ${type eq "4" ? "Selected" : ""}>
                                            Từ chối
                                        </option>
                                        <option class="text-center " value="registedService?${historyKey}&type=2" ${type eq "2" ? "Selected" : ""}>
                                            Chờ xử lý
                                        </option> 
                                    </select>
                                </div><br>
                            </div>

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Customer</th>
                                        <th scope="col">Technical Staff</th>
                                        <th scope="col">Service</th>
                                        <th scope="col">Time slot</th>
                                        <th scope="col">Register Date</th>
                                        <th scope="col">Start date</th>
                                        <th scope="col">End date</th>
                                        <th scope="col">Note</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Seller</th>
                                        <th scope="col">Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <c:forEach items="${listOfPage}" var="r">
                                            <th scope="row" name="regist_id">${r.regist_id}</th>
                                            <td>${r.customer.fullName}</td>
                                            <td>${r.technicalStaff.fullName}</td>
                                            <td>${r.service.service_name}</td>
                                            <td>${r.timeslot.ts_description}</td>
                                            <td>${r.regist_date}</td>
                                            <td>${r.start_date}</td>
                                            <td>${r.end_date}</td>
                                            <td>${r.note}</td>
                                            <td>
                                                <c:if test="${r.status_id==3}">
                                                    <font color="green"> Chấp nhận
                                                </c:if>
                                                <c:if test="${r.status_id==4}">
                                                    <font color="red"> Từ chối
                                                </c:if>
                                                <c:if test="${r.status_id==2}">
                                                    <font color="gray"> Chờ xử lý
                                                </c:if>
                                            </td>
                                            <td>${r.seller.fullName}</td>
                                            <td>
                                                <a class="btn btn-success" href="acceptRegist?regist_id=${r.regist_id}" role="button">Chấp nhận</a>
                                                <a class="btn btn-danger" href="denyRegist?regist_id=${r.regist_id}" role="button">Từ chối</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!-- pagination -->
        <c:if test="${listOfPage.size() !=0}">
            <nav aria-label="..." class="pagination">
                <ul class="pagination">
                    <li class="page-item">
                        <a <c:if test="${pageIndex!=1}">                         
                                href="registedService?index=${pageIndex-1}${historyKey}${historyType}"
                            </c:if> class="page-link" aria-label="Previous">
                            <span  aria-hidden="true">«</span>
                        </a>
                    </li>

                    <c:forEach begin="1" end="${endPage}" var="i">
                        <li class="page-item ${i==pageIndex ?"active" : ""}">
                            <a class="page-link" href="registedService?index=${i}${historyKey}${historyType}">${i}</a>
                        </li>
                    </c:forEach>

                    <li class="page-item">
                        <a <c:if test="${pageIndex!=endPage}">
                                href="registedService?index=${pageIndex+1}${historyKey}${historyType}"
                            </c:if> class="page-link" aria-label="Next">
                            <span aria-hidden="true">»</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </c:if>
    </body>
    <script>
        function clearInput() {
            document.getElementById("myInput1").value = "";
        }
    </script>
</html>
