
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>



<!-- Notification User -->
<c:if test="${notification !=null}">
    <div class="alert ${notification.toLowerCase().contains("thành công") ? "alert-success" : "alert-danger"} alert-dismissible fade show " role="alert" style="position: fixed; z-index: 15 ; margin-left: 40%">
        <strong>${notification}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</c:if>

<c:if test="${sessionScope.message !=null}">
    <div class="alert ${sessionScope.message.toLowerCase().contains("thành công") ? "alert-success" : "alert-danger"} alert-dismissible fade show " role="alert" style="position: fixed; z-index: 15 ; margin-left: 40%">
        <strong>${sessionScope.message}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <c:remove var="message" scope="session" />
</c:if>

<!-- Login -->
<div class="modal fade col-md-12" role="dialog" id="loginModal">
    <div class="modal-dialog">
        <div class="modal-content" style="border-radius: 10px; margin-top: 258px;">
            <div class="modal-header">
                <h2 class="" id="loginModal" style="text-align:center; font-family: Arial"><b style="padding-left: 150px;">Đăng Nhập</b></h2><br>
            </div>
            <div class="modal-body">
                <form action="login" method="post">
                    <div class="form-group">
                        <input name="email" type="email" class="form-control" placeholder="Email của bạn"style="border-radius: 100px;" required="">
                    </div>
                    <div class="form-group">
                        <input name="password" type="password" class="form-control" placeholder="Mật khẩu" style="border-radius: 100px;" required>
                    </div>
                    <center><button type="submit" class="btn btn-dark" style="padding-right: 193px;padding-left: 193px; border-radius: 100px;">Đăng nhập</button></center>
                    <center>
                        <a class="btn btn-lg btn-google btn-block text btn-outline " style="border-radius: 150px;"
                           href="https://accounts.google.com/o/oauth2/auth?scope=email%20profile 
                           &redirect_uri=http://localhost:8080/SWP391_Fitnesscare/logingoogle&response_type=code
                           &client_id=658277419145-hml20trk79j37nvtdpb7qja08dlppd0u.apps.googleusercontent.com&approval_prompt=force">
                            <img src="https://img.icons8.com/color/16/000000/google-logo.png"> Login with Google
                        </a>
                    </center>

                </form>
                <br><br>
                <a type="button" data-toggle="modal" data-dismiss="modal" data-target="#ResetPasswordModal" style="float:right; text-decoration: none; border-radius: 100px;">Quên Mật Khẩu</a>
                <a type="button" data-toggle="modal" data-dismiss="modal" data-target="#registerModal" style="float:left; text-decoration: none; border-radius: 100px;">Đăng ký tài khoản mới</a>
            </div>
        </div>
    </div>
</div>


<!-- user profile -->
<div class="modal fade col-md-12" role="dialog" id="userProfileModal" style="padding-right: 18px" >
    <div class="modal-dialog">
        <div class="modal-content" style="border-radius: 10px; margin-top: 10px; min-width: 1150px; margin-left: -320px;">
            <div class="modal-header">
                <h2 class="" id="userProfileModal" style="text-align:center; font-family: Arial"><b style="padding-left: 400px;"> Thông tin cá nhân</b></h2><br>
            </div>

            <div class="modal-body">
                <section>
                    <div class="container">

                        <div class="row">
                            <c:if test="${sessionScope.us.avatar != null && sessionScope.us.avatar ne ''}">
                                <div class="col-md-4">
                                    <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                        <img class="rounded-circle mt-5" width="150px" height="150px" src="${sessionScope.us.avatar}">
                                        <span class="font-weight-bold">${sessionScope.us.fullName}</span>
                                        <span class="text-black-50">${sessionScope.us.email}</span>
                                    </div>
                                </div>
                            </c:if>
                            <c:if test="${sessionScope.us.avatar == null || sessionScope.us.avatar eq ''}">
                                <div class="col-md-4">
                                    <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                        <img class="rounded-circle mt-5" width="150px" height="150px" src="./images/avatar/avtdefault.png"/>
                                        <span class="font-weight-bold">${sessionScope.us.fullName}</span><span class="text-black-50">${sessionScope.us.email}</span>
                                    </div>
                                </div>
                            </c:if>

                            <div class="col-md-8">
                                <div class="p-3 py-5">
                                    <form action="editprofile" method="post" enctype="multipart/form-data"  >

                                        <div class="row mt-4">
                                            <div class="row mt-4 col-md-6">
                                                <label class="labels" style="font-size: 10px;">Họ và tên</label>
                                                <input type="text" class="form-control" name="fullName" placeholder="Họ và tên" value="${sessionScope.us.fullName}">
                                            </div>&nbsp;
                                            <div class="row mt-4 col-md-6">
                                                <label class="labels" style="font-size: 10px;">Số điện thoại</label>
                                                <input type="text" class="form-control" name="phone" placeholder="Số điện thoại" value="${sessionScope.us.phone}">
                                            </div>&nbsp;
                                            <div class="row mt-4 col-md-6">
                                                <label class="labels" style="font-size: 10px;">Địa chỉ</label>
                                                <input type="text" class="form-control" name="address" placeholder="Địa chỉ" value="${sessionScope.us.address}">
                                            </div>&nbsp;
                                            <div class="row mt-4 col-md-6">
                                                <label class="labels" style="font-size: 10px;">Email</label>
                                                <input type="text" class="form-control" name="email" placeholder="Email" value="${sessionScope.us.email}">
                                            </div>&nbsp;
                                            <div class="row mt-4 col-md-6">
                                                <label class="labels" style="font-size: 10px;">Ảnh đại diện</label>
                                                <input type="file" class="form-control" accept="image/*" name="avatar" placeholder="Ảnh đại diện" value="${sessionScope.us.avatar}">
                                            </div>&nbsp;
                                            <div class="row mt-4 col-md-3"><label class="labels" style="font-size: 10px;" name="gender" value="${sessionScope.us.gender}">Giới tính</label>
                                                <div>
                                                    <span>
                                                        <input name="gender" type="radio" value="1" ${sessionScope.us.gender  ? 'checked' : ''}/>  Nam
                                                    </span> &nbsp; &nbsp; &nbsp;
                                                    <span>
                                                        <input name="gender" type="radio" value="0" ${!sessionScope.us.gender  ? 'checked' : ''}/>  Nữ
                                                    </span> 
                                                </div>               
                                            </div>


                                            <input type="hidden" name="user_id" value="${sessionScope.us.user_id}"/>
                                            <c:if test="${sessionScope.us.password.length() ne  0}">
                                                <div class="row mt-4 col-md-3">
                                                    <label class="labels" style="font-size: 10px;">Mật khẩu</label>
                                                    <a href="#" style="text-decoration: none;">
                                                        <button type="button" data-toggle="modal" data-dismiss="modal" data-target="#ChangePasswordModal"  class="btn btn-dark" value="">Đổi mật khẩu</button>
                                                    </a>
                                                </div>
                                            </c:if>
                                        </div>
                                        <div class="row mt-5 col-md-6 text-center">
                                            <button class="btn btn-dark" type="submit">Lưu</button>
                                        </div>
                                        <input type="hidden" class="form-control" name="password"  value="${sessionScope.us.password}">
                                    </form>
                                    <div class="row mt-5 col-md-6 text-center d-flex ">
                                        <a type="button" href="home" type="button" data-toggle="modal" 
                                           data-dismiss="modal" style="padding-left: 150px; text-decoration: none;
                                           border-radius: 100px;" >
                                            Quay về trang chủ
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div> 


<!-- Change Password -->
<div class="modal fade col-md-12" role="dialog" id="ChangePasswordModal">
    <div class="modal-dialog">
        <div class="modal-content" style="border-radius: 10px; margin-top: 150px;">
            <div class="modal-header">
                <h1 style="text-align: center ; margin-left: 50px;">Thay đổi mật khẩu</h1>
            </div>
            <div class="modal-body">
                <form action="changepassword" method="post">
                    <input type="hidden" name="user_id" value="${sessionScope.us.user_id}"/>
                    <b>Mật khẩu cũ</b>&nbsp;&nbsp;
                    <!--                                <i onclick="changeTypeOll_Pass()" class="fa fa-eye icon"></i>-->
                    <div class="form-group">
                        <input name="old_pass" type="password" class="form-control" placeholder="Mật khẩu cũ"style="border-radius: 100px;" required="">
                    </div>
                    <b>Mật khẩu mới</b>&nbsp;&nbsp;
                    <!--<i onclick="changeTypeNew_Pass1()" class="fa fa-eye icon"></i>-->
                    <div class="form-group">
                        <input name="new_pass1" type="password" class="form-control" placeholder="Mật khẩu mới"style="border-radius: 100px;" required>
                    </div>
                    <b>Nhập lại mật khẩu mới</b>&nbsp;&nbsp;
                    <!--<i onclick="changeTypeNew_Pass2()" class="fa fa-eye icon"></i>-->
                    <div class="form-group">
                        <input name="new_pass2" type="password" class="form-control" placeholder="Nhập lại mật khẩu mới"style="border-radius: 100px;" required>
                    </div>
                    <br>
                    <center><button type="submit" class="btn btn-dark" style="padding-right: 160px;padding-left: 160px; border-radius: 100px;">Cập nhập mật khẩu</button></center>
                </form>
                <br><br>
                <a type="button" data-toggle="modal" data-dismiss="modal" data-target="#userProfileModal" style="padding-left: 190px; text-decoration: none; border-radius: 100px;">Quay lại </a> 
            </div>
        </div>
    </div>
</div>


<!-- Register -->

<div class="modal fade col-md-12" role="dialog" id="registerModal" style="padding-right: 18px" >
    <div class="modal-dialog">
        <div class="modal-content" style="border-radius: 10px; margin-top: 70px;">
            <div class="modal-header">
                <h2 class="" id="registerModal" style="text-align:center; font-family: Arial"><b style="padding-left: 100px;">Đăng ký tài khoản</b></h2><br>
            </div>

            <div class="modal-body">
                <section>
                    <div class="container">
                        <form action="register" method="POST">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" placeholder="Họ và tên" name="fullName" style="border-radius: 100px;" required>
                                    </div></div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" style="border-radius: 100px;" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="phone" placeholder="Số điện thoại" name="phone" style="border-radius: 100px;" required>
                                    </div></div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="pwd" placeholder="Mật khẩu" name="password" style="border-radius: 100px;" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="repwd" placeholder="Nhập lại mật khẩu" name="repassword" style="border-radius: 100px;" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group" required>
                                        Giới tính&nbsp;&nbsp;&nbsp;
                                        <input class="" name="gender" type="radio" value="True" required/>&nbsp; Nam
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <input class="" name="gender" type="radio" value="False" required/>&nbsp; Nữ
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="address" placeholder="Nhập địa chỉ" name="address" style="border-radius: 100px;" required>
                                    </div>
                                </div>
                                <br><br><center><button type="submit" class="btn btn-dark" style="padding-right: 190px;padding-left: 190px; border-radius: 100px;">Đăng ký</button></center><br><br>
                            </div>
                        </form>
                        <a type="button" data-toggle="modal" data-dismiss="modal" data-target="#loginModal" style="padding-left: 150px; text-decoration: none; border-radius: 100px;">Quay lại đăng nhập</a> 
                    </div>    
                </section>
            </div>
        </div>
    </div>
</div> 


<!-- ResetPassword -->
<div class="modal fade col-md-12" role="dialog" id="ResetPasswordModal">
    <div class="modal-dialog">
        <div class="modal-content" style="border-radius: 10px; margin-top: 150px;">
            <div class="modal-header">
                <h1 style="text-align: center ; margin-left: 70px;">Cấp lại mật khẩu</h1>
            </div>
            <div class="modal-body">
                <form action="resetpassword" method="post">
                    <div class="form-group">
                        <input name="email" type="email" class="form-control" placeholder="Email của bạn"style="border-radius: 100px;" required>
                    </div>
                    <br>
                    <center>
                        <button type="submit" class="btn btn-dark" style="padding-right: 200px;
                                padding-left: 200px;
                                border-radius: 100px;
                                margin-bottom: -40px;
                                height: 50px;">Gửi
                        </button>
                    </center>
                </form>
                <br><br>
                <a type="button" data-toggle="modal" data-dismiss="modal" data-target="#loginModal" style="padding-left: 170px; text-decoration: none; border-radius: 100px;">Quay lại Đăng nhập</a> 
            </div>
        </div>
    </div>
</div>

<script>
    if (document.querySelector('.alert')) {
        document.querySelectorAll('.alert').forEach(function ($el) {
            setTimeout(() => {
                $el.classList.remove('show');
            }, 2000);
        });
    }
</script>
